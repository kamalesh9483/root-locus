clear;
clc;
% Getting OLTF from user
s = tf('s')
disp("Enter the OLTF mentioning as s\n")
OLTF = input('s')

% Extracting Numerator (num) and Denominator (den) terms from OLTF
[num ,den] = tfdata(OLTF)
H = tf(num,den);

% Plotting Root Locus
rlocus(H,'b')
[Gm,Pm,Wcg,Wcp] = margin(H)

% Printing the results
fprintf("The results indicate that a gain variation of over %f dB at the gain crossover frequency of %f rad/s would cause the system to be unstable.",Gm,Wcg);
fprintf("Similarly a phase variation of over %f degrees at the phase crossover frequency of %f rad/s will cause the system to lose stability.",Pm,Wcp);

sgrid